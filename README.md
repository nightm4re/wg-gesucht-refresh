# wg-gesucht-refresh
This script periodically updates listings on wg-gesucht.de. Once started, it will run indefinitely until interrupted. As the process blocks out resources permanently, this script is best used on small single-purpose machines.
## Usage
1. Install Python 3.7+
2. Install dependencies:
    * [playwright-python](https://playwright.dev/python/docs/intro): first, do `pip install playwright` to install the library, then `playwright install` to initialize browsers. 
3. Run script with ad id arguments: `python ./wg-gesucht-refresh.py --ad-id 8304305 8860268`
4. Input username (mail) and password

![](screenshot.png)