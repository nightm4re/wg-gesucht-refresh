import argparse
import getpass
import logging
import random
import time

from playwright.sync_api import sync_playwright, Page


def sleep(min_secs, max_secs):
    sleep_time = random.uniform(min_secs, max_secs)
    logging.info(f"Sleeping {sleep_time:.2f} seconds...")
    time.sleep(sleep_time)
    logging.info("Slept enough!")


def update(page: Page, ad_id: str):
    logging.info(f"Updating ad {ad_id}...")
    try:
        page.goto(f"https://www.wg-gesucht.de/angebot-bearbeiten.html?action=update_offer&offer_id={ad_id}")
        page.locator("#update_offer").click()
        logging.info(f"Updating ad {ad_id} successful!")
    except:
        logging.exception(f"Updating ad {ad_id} failed!")


if __name__ == "__main__":
    logging.basicConfig(format='%(asctime)s\t[%(levelname)s]\t%(message)s', level=logging.INFO)

    parser = argparse.ArgumentParser(description='Keep WG-Gesucht.de ads on top of the listing by regularly toggling their activation status.')
    parser.add_argument("--ad-id", nargs='+', help="")
    args = parser.parse_args()
    username = input("username:")
    password = getpass.getpass("password:")
    logging.info(f"Parsed ad id arguments: {','.join(args.ad_id)}")

    while True:
        with sync_playwright() as playwright:
            firefox = playwright.firefox
            browser = firefox.launch(headless=True, slow_mo=1000)
            page = browser.new_page()
            try:
                page.goto("http://www.wg-gesucht.de/mein-wg-gesucht.html")
                page.locator("#login_email_username").fill(username)
                page.locator("#login_basic #login_password").fill(password)
                page.get_by_role("button", name="Login").click()
                page.get_by_role("button", name="Save").click()
                logging.info("Login successful!")
                sleep(5, 20)
            except Exception as e:
                logging.exception("Login failed!")
            logging.info("Updating ads...")
            for ad in args.ad_id:
                update(page, ad)
                sleep(5, 20)

            browser.close()

        sleep(1800, 3600)
